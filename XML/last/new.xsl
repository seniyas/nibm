<?xml version="1.0" encoding="utf-8"?><!-- DWXMLSource="catalog.xml" -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
<xsl:template match="/">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Untitled Document</title>
</head>

<body>
<h1>My CD collection</h1>
<table border="2">
<tr>
<th>TITLE</th>
<th>ARISTS</th>
<th>Country</th>
<th>Company</th>
<th>Price</th>
<th>Year</th>
</tr>

<xsl:for-each select="catalog/cd">
<tr>


<td><xsl:value-of select="title"/></td>
<td><xsl:value-of select="artist"/></td>
<td><xsl:value-of select="company"/></td>
<td><xsl:value-of select="country"/></td>
<td><xsl:value-of select="price"/></td>
<td><xsl:value-of select="year"/></td>

</tr>
</xsl:for-each>

</table>
</body>
</html>

</xsl:template>
</xsl:stylesheet>